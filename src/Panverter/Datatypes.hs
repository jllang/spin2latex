module Panverter.Datatypes where

import Data.Text.Lazy
import Data.List.NonEmpty
import Text.Regex.TDFA
import Text.Regex.TDFA.Text ()

import Panverter.Model

data Arguments        = Arguments
                      { getDirectory     :: FilePath
                      , getOutputFile    :: FilePath
                      , getExponent      :: Int
                      , getFormat        :: Format
                      , getStylesheet    :: String
                      , getIndentation   :: Int
                      , getHideModelName :: Bool
                      , getTokenWord     :: String
                      }
data Format           = CSV
                      | Haskell
                      | HTML
                      | JSON
                      | Latex
                      | Markdown
                      | SQLite
                      | XML
                      deriving Show
newtype Stylesheet    = Stylesheet String
newtype Indentation   = Indentation Int
data NumberFormat     = Fixed | Floating
newtype Denominator   = Denominator Double
newtype Position      = Position Int
data TokenSymbol      = TokenSymbol
                      { getSymbol   :: Char
                      , getPosition :: Position
                      }
type TokenWord        = NonEmpty TokenSymbol
data Token            = Token Regex ValueType TokenType
type Tokens           = NonEmpty Token
type Source           = Int
type Target           = Int
type Permutation      = NonEmpty (Source, Target)
newtype HideModelName = HideModelName Bool
newtype Match         = Match Text
newtype Rest          = Rest  Text
type Models           = NonEmpty Model
data StaticOptions    = StaticOptions MissingValue NumberFormat
data DynamicOptions   = DynamicOptions
                        HideModelName
                        Denominator
                        (Maybe Stylesheet)
                        Indentation
data Output           = Output Format DynamicOptions [Model]
newtype MissingValue  = MissingValue Text
newtype Headings      = Headings (NonEmpty Heading)
type Heading          = Text
type Prefix           = Text
type Infix            = Text
type Postfix          = Text
data Delimiters       = Delimiters Prefix Infix Postfix
type OutputProcessor  = DynamicOptions -> NonEmpty Model -> Text

instance Eq TokenSymbol where
  (TokenSymbol _ (Position x)) == (TokenSymbol _ (Position y)) = x == y

instance Ord TokenSymbol where
  (TokenSymbol _ (Position x)) <= (TokenSymbol _ (Position y)) = x <= y
